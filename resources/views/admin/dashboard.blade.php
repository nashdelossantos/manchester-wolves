@extends('admin.masters.base')

@section('pagetitle')
	Dashboard <small>Shop overview</small>
@stop

@section('crumbs')
	<li class="active">
		<i class="fa fa-dashboard"></i> Dashboard
	</li>
@stop

@section('pagecontents')
	<div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-copy fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">26</div>
                            <div>Total Pages</div>
                        </div>
                    </div>
                </div>
                <a href="{{ url('/admin/pages') }}">
                    <div class="panel-footer">
                        <span class="pull-left">View Pages</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-align-left fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">124</div>
                            <div>Total News</div>
                        </div>
                    </div>
                </div>
                <a href="{{ url('/admin/news') }}">
                    <div class="panel-footer">
                        <span class="pull-left">View News</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>

@stop