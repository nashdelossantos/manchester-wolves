@extends('admin.masters.base')

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/css/awesome-bootstrap-checkbox.css') }}">
@stop

@section('pagetitle')
	Page <small>edit</small>
@stop

@section('crumbs')
	<li><a href="{{ url('admin/pages') }}"><i class="fa fa-text-o"></i> Pages</a></li>
	<li class="active"><i class="fa fa-pencil"></i> Editing a page</li>
@stop

@section('pagecontents')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left"><i class="fa fa-th fa-fw"></i> Create New Page</h3>
                    <p class="pull-right">
                    	<a href="{{ url('admin/pages') }}" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> Cancel</a>
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        {!! Form::model($page, ['url' => '/admin/pages/' . $page->slug, 'method' => 'PATCH', 'files' => true]) !!}
                			<div class="col-md-8">
                				
            					<div class="form-group">
            						{!! Form::label('page_title', 'Page Title', ['class' => 'control-label']) !!}
									{!! Form::text('page_title', null, ['class' => 'form-control', 'required']) !!}
            					</div>

                                <div class="form-group">
                                    {!! Form::label('heading', 'Heading', ['class' => 'control-label']) !!}
                                    {!! Form::text('heading', null, ['class' => 'form-control']) !!}
                                </div>
    								
                			</div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('page_content', 'Description', ['class' => 'control-label']) !!}
                                    {!! Form::textarea('page_content', null, ['class' => 'form-control', 'rows' => '12', 'id' => 'desc_content']) !!}
                                </div>

                                <div class="form-group">
                                    <div class="checkbox checkbox-success">
                                        <input type="checkbox" id="status" name="active">
                                            <label for="checkbox1"> Tick to make public
                                        </label>
                                    </div>
                                </div>

                                <h3>Metadata </h3>
                                <div class="form-group">
                                    {!! Form::label('meta_title', 'Meta Title', ['class' => 'control-label']) !!}
                                    {!! Form::text('meta_title', null, ['class' => 'form-control', 'rows' => '12']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('meta_keywords', 'Meta Keywords', ['class' => 'control-label']) !!}
                                    {!! Form::text('meta_keywords', null, ['class' => 'form-control', 'rows' => '12']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('meta_description', 'Meta Description', ['class' => 'control-label']) !!}
                                    {!! Form::textarea('meta_description', null, ['class' => 'form-control', 'rows' => '12']) !!}
                                </div>

                                <div class="form-group text-right">
                                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript" src="{{ asset('/js/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript">
        CKEDITOR.replace('desc_content');
    </script>
@stop

