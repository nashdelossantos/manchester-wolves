@extends('admin.masters.base')

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/css/awesome-bootstrap-checkbox.css') }}">
@stop

@section('pagetitle')
	Setting <small>edit</small>
@stop

@section('crumbs')
	<li><a href="{{ url('admin/settings') }}"><i class="fa fa-cogs"></i> Settings</a></li>
	<li class="active"><i class="fa fa-pencil"></i> Editing a Setting</li>
@stop

@section('pagecontents')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left"><i class="fa fa-th fa-fw"></i> Editing a Setting</h3>
                    <p class="pull-right">
                    	<a href="{{ url('admin/settings') }}" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> Cancel</a>
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        {!! Form::model($setting, ['url' => '/admin/settings/' . $setting->slug, 'method' => 'PATCH']) !!}
                			<div class="col-md-8">
                				
            					<div class="form-group">
            						{!! Form::label('setting_name', 'Setting Title', ['class' => 'control-label']) !!}
									{!! Form::text('setting_name', null, ['class' => 'form-control', 'required']) !!}
            					</div>
    								
                			</div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('setting_value', 'Value', ['class' => 'control-label']) !!}
                                    {!! Form::text('setting_value', null, ['class' => 'form-control', 'required']) !!}
                                </div>

                                <div class="form-group">
                                    <div class="checkbox checkbox-success">
                                        {!! Form::checkbox('active') !!}
                                            <label for="checkbox1"> Tick to make public
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group text-right">
                                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript" src="{{ asset('/js/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript">
        CKEDITOR.replace('desc_content');
    </script>
@stop

