@extends('admin.masters.base')

@section('pagetitle')
	Profile <small>overview</small>
@stop

@section('crumbs')
	<li class="active">
		<i class="fa fa-user"></i> Profile
	</li>
@stop

@section('pagecontents')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left"><i class="fa fa-th fa-fw"></i> Profile Panel</h3>
                    <p class="pull-right">
                        <a href="{{ url('admin/profile') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Cancel</a>
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body pages">
                        <div class="table-responsive">
                            {!! Form::model($profile, ['url' => '/admin/profile/' . $profile, 'method' => 'PATCH']) !!}
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>{!! Form::text('name', null, ['class' => 'form-control']) !!}</th>
                                    </tr>
                                </thead>
                                <tbody>
	                                <tr>
                                        <td>Email</td>
	                                    <td>{!! Form::email('email', null, ['class' => 'form-control']) !!}</td>
	                                </tr>
                                    <tr>
                                        <td>Password</td>
                                        <td>{!! Form::password('password', ['class' => 'form-control', 'required']) !!}</td>
                                    </tr>
                                </tbody>


                            </table>
                                <div class="text-right">{!! Form::submit('Update Profile', ['class' => 'btn btn-primary']) !!}</div>
                            {!! Form::close() !!}
                        </div>
                </div>
            </div>
        </div>
    </div>
@stop