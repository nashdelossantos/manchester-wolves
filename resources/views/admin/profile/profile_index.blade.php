@extends('admin.masters.base')

@section('pagetitle')
	Profile <small>overview</small>
@stop

@section('crumbs')
	<li class="active">
		<i class="fa fa-user"></i> Profile
	</li>
@stop

@section('pagecontents')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left"><i class="fa fa-th fa-fw"></i> Profile Panel</h3>
                    <p class="pull-right">
                        <a href="{{ url('admin/profile/profile/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Edit Profile</a>
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body pages">
                    @if ($profile == '')
                        <div class="alert alert-info">Profile list is currently empty. </div>
                    @else
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Value</th>
                                    </tr>
                                </thead>
                                <tbody>
	                                <tr>
                                        <td>Name</td>
	                                    <td>{{ $profile->name }}</td>
	                                </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>{{ $profile->email }}</td>
                                    </tr>
                                    <tr>
                                        <td>Password</td>
                                        <td><em>(click edit to change password)</em></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop