@extends('admin.masters.base')

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/css/awesome-bootstrap-checkbox.css') }}">
@stop

@section('pagetitle')
	Products <small>new entry</small>
@stop

@section('crumbs')
	<li><a href="{{ url('admin/products') }}"><i class="fa fa-th"></i> Products</a></li>
	<li class="active"><i class="fa fa-pencil"></i> New Product</li>
@stop

@section('pagecontents')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left"><i class="fa fa-th fa-fw"></i> Create New Product</h3>
                    <p class="pull-right">
                    	<a href="{{ url('admin/products') }}" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> Cancel</a>
                    </p>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        {!! Form::model($product, ['url' => '/admin/products/' . $product->slug, 'method' => 'PATCH', 'files' => true]) !!}
                            <div class="col-md-5">
                                
                                <div class="form-group">
                                    {!! Form::label('category_id', 'Category Name', ['class' => 'control-label']) !!}
                                    {!! Form::select('category_id', $categories, null, ['class' => 'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('product_name', 'Product Name', ['class' => 'control-label']) !!}
                                    {!! Form::text('product_name', null, ['class' => 'form-control', 'required']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('colour', 'Colour', ['class' => 'control-label']) !!}
                                    {!! Form::text('colour', null, ['class' => 'form-control', 'required']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('dimension', 'Dimension', ['class' => 'control-label']) !!}
                                    {!! Form::text('dimension', null, ['class' => 'form-control', 'required']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('weight', 'Weight', ['class' => 'control-label']) !!}
                                    {!! Form::text('weight', null, ['class' => 'form-control', 'required']) !!}
                                </div>
                                    
                                <div class="form-group">
                                    {!! Form::label('quantity', 'Quantity', ['class' => 'control-label']) !!}
                                    <div class="input-group spinner">
                                        {!! Form::text('quantity', null, ['class' => 'form-control']) !!}
                                        <div class="input-group-btn-vertical">
                                            <div class="btn btn-default"><i class="fa fa-caret-up"></i></div>
                                            <div class="btn btn-default"><i class="fa fa-caret-down"></i></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('amount', 'Amount', ['class' => 'control-label']) !!}
                                    {!! Form::text('amount', null, ['class' => 'form-control', 'required']) !!}
                                </div>

                                <div class="form-group">
                                    <div class="checkbox checkbox-success">
                                        {!! Form::checkbox('active', null, ['class' => 'form-control']) !!}
                                            <label for="checkbox1"> Tick to make public
                                        </label>
                                    </div>
                                </div>
                                    
                                    
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    {!! Form::label('product_image', 'Product Image', ['class' => 'control-label']) !!}
                                    {!! Form::file('product_image') !!}
                                  
                                </div>
                                <div class="form-group">
                                    {!! Form::label('product_description', 'Product Description', ['class' => 'control-label']) !!}
                                    {!! Form::textarea('product_description', null, ['class' => 'form-control', 'rows' => '18']) !!}
                                </div>

                                <div class="form-group text-right">
                                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    
@stop

@section('scripts')
    <script>
            $(document).ready(function(){
        //Stop people from typing
        $('.spinner input').keydown(function(e){
            e.preventDefault();
            return false;
        });
        var minNumber = 1;
        var maxNumber = 10;
        $('.spinner .btn:first-of-type').on('click', function() {
            if($('.spinner input').val() == maxNumber){
                return false;
            }else{
                $('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
            }
        });

        $('.spinner .btn:last-of-type').on('click', function() {
            if($('.spinner input').val() == minNumber){
                return false;
            }else{
                $('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
            }
        });
    });
    </script>
@stop


