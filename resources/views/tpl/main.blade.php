<!DOCTYPE html>
<html>
<head>
	<title>Manchester Wolves</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<link rel="stylesheet" type="text/css" href="{{ asset( 'css/bootstrap.min.css' ) }}">
	<link rel="stylesheet" type="text/css" href="{{ asset( 'css/font-awesome.min.css' ) }}">
	<link rel="stylesheet" type="text/css" href="{{ asset( 'css/style.css' ) }}">

	@yield( 'styles' )

	<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('img/apple-icon-57x57.png') }}">
	<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('img/apple-icon-60x60.png') }}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/apple-icon-72x72.png') }}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/apple-icon-76x76.png') }}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/apple-icon-114x114.png') }}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('img/apple-icon-120x120.png') }}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('img/apple-icon-144x144.png') }}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('img/apple-icon-152x152.png') }}">
	<link rel="apple-touch-icon" sizes="180x180" href="/{{ asset('imgapple-icon-180x180.png') }}">
	<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('img/android-icon-192x192.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon-32x32.png') }}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('img/favicon-96x96.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon-16x16.png') }}">
	<link rel="manifest" href="{{ asset('img/manifest.json') }}">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="{{ asset('img/ms-icon-144x144.png') }}">
	<meta name="theme-color" content="#ffffff">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	<header>
		<div class="header-wrapper">
			<section class="navbar bs-docs-nav" id="top" role="banner">
				<div class="container" id="top-sub-text">
					<a href="{{ url( '/' ) }}" class="navbar-brand">
						<img src="{{ asset( 'img/logo.png' ) }}">
					</a>
					<div class="secondary-navigation">
						<span>Wolves Official Website</span>
						<ul class="navbar navbar-right hidden">
							<li><a href="#">Wolves Wall</a></li>
							<li><a href="#">Wolves Player</a></li>
							<li><a href="#">Shop</a></li>
							<li><a href="#">Newsletter</a></li>
							<li><a href="#">Switch to Mobile</a></li>
						</ul>
					</div>					
				</div>
				<div class="main-navigation">
					<div class="container" id="main-menu-container">
						<div class="navbar-header">
							<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<nav class="navbar-collapse bs-navbar-collapse collapse" id="main-menu-wrapper" aria-expanded="false" style="height: 1px;">
							<ul class="nav navbar-nav navbar-right" id="main-menu">
								<li @if(isset($pagename) &&$pagename == 'about' ) class="active" @endif >
									<a href="{{ url( 'about' ) }}">About Us</a>
								</li>
								<li @if(isset($pagename) && $pagename == 'news' ) class="active" @endif >
									<a href="{{ url( 'news' ) }}">News</a>
								</li>
								<li @if(isset($pagename) && $pagename == 'fixtures' ) class="active" @endif >
									<a href="{{ url( 'fixtures' ) }}">Fixtures</a>			
								</li>
								<li @if(isset($pagename) && $pagename == 'results' ) class="active" @endif >
									<a href="{{ url( 'results' ) }}">Results</a>
								</li>
								<li @if(isset($pagename) && $pagename == 'contact-us' ) class="active" @endif >
									<a href="{{ url( 'contact-us' ) }}">Contact Us </a>
								</li>
								<li @if(isset($pagename) && $pagename == 'ground' ) class="active" @endif >
									<a href="{{ url( 'ground' ) }}">Our Ground</a>
								</li>

								@if (Auth::guest())
									<li><a href="{{ url('/auth/login') }}"><i class="fa fa-key"></i> Login </a></li>
								@else

									@if (Auth::user()->hasRole(['owner', 'admin']))
										<li class="dropdown">
			                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cogs"></i> Admin <span class="caret"></span></a>
			                                <ul class="dropdown-menu">
			                                    <li><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
			                                    <li role="separator" class="divider"></li>
			                                    <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
			                                </ul>
			                            </li>
									@else
										<li class="dropdown">
			                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> Profile <span class="caret"></span></a>
			                                <ul class="dropdown-menu">
			                                    <li><a href="{{ url('user/edit-profile') }}"> Edit Profile</a></li>
			                                    <li role="separator" class="divider"></li>
			                                    <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
			                                </ul>
			                            </li>
									@endif
								@endif
							</ul>						
						</nav>
					</div>					
				</div>
				<div class="container">
					<div class="bottom-sub-text hidden">
						<p>Official Website Sponsor</p>
					</div>
				</div>
			</section>
		</div>
	</header>
	<!-- End: header -->

	<!-- #main-container -->
	<section id="main-container">
		<div class=" main-content">
			<div class="container-fluid remove-side-padding">
				<div class="col-md-12 remove-side-padding left-container">
					<!-- page-content -->
					<div class="page-content">
						@yield( 'slider' )
						@yield( 'pagecontent' )
					</div>
					<!-- End: page-content -->


				</div>
			</div>
		</div>
	</section>

	<section id="media-section">
		<div class="container col-md-12 official-sites-container">
			<div class="container official-sites-content">
				<div class="col-md-12 content">
					<ul class="list-inline bottom-list pull-left">
						<li><img src="{{ asset( 'img/logo.png' ) }}" class="logo"></li>
						<li><p>THE OFFICIAL CLUB WEBSITE OF WOLVES</p></li>
					</ul>
					<ul class="list-inline bottom-social-links pull-right">
						<li>
							<a href="#"><i class="fa fa-twitter-square"></i> Follow us on Twitter</a>
						</li>
						<li class="list-fb">
							<a href="#"><i class="fa fa-facebook-square"></i> Find us on Facebook</a>
						</li>
						<li class="list-rss">
							<a href="#"><i class="fa fa-rss-square"></i> RSS updates</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</section>

	<section id="bottom-links-horizontal-1">
		<div >
			<div class="container">
				<ul class="list-inline">
					<li><a href="#">Terms of Use</a></li>
					<li><a href="#">Privacy Policy</a></li>
					<li><a href="#">Privacy Policy</a></li>
					<li><a href="#">Accessibility</a></li>
					<li><a href="#">Company Details</a></li>
					<li><a href="#">Careers</a></li>
					<li><a href="#">Contact</a></li>
					<li><a href="#">Unsubscribe</a></li>
					<li><a href="#">CEOP Report Abuse</a></li>
					<li><a href="#">Switch to Mobile</a></li>
				</ul>					
			</div>
		</div>
		<div class="clearfix"></div>
	</section>

	<!-- footer -->
	<footer>
		<div class="container">
			<div class="links">
				<ul class="list-inline">
					<li>
						<a href="#" class="heading">News</a>
						<ul class="list-unstyled sub-links">
							<li>
								<a href="#">First Team</a>
							</li>
							<li>
								<a href="#">Latest Video</a>
							</li>
							<li>
								<a href="#">Club News</a>
							</li>
							<li>
								<a href="#">Academy</a>
							</li>
							<li>
								<a href="#">Wolves Women</a>
							</li>
							<li>
								<a href="#">Development Centre</a>
							</li>
							<li>
								<a href="#">Archive News</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#" class="heading">Fixtures and Results</a>
						<ul class="list-unstyled sub-links">
							<li>
								<a href="#">League Table</a>
							</li>
							<li>
								<a href="#">Fixtures List</a>
							</li>
							<li>
								<a href="#">Match Preview</a>
							</li>
							<li>
								<a href="#">Match Report</a>
							</li>
							<li>
								<a href="#">U21s Fixtures</a>
							</li>
							<li>
								<a href="#">U21s Report</a>
							</li>
							<li>
								<a href="#">Academy Fixtures</a>
							</li>
							<li>
								<a href="#">Academy Report</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#" class="heading">Tickets</a>
						<ul class="list-unstyled sub-links">
							<li>
								<a href="#">Latest Ticket News</a>
							</li>
							<li>
								<a href="#">Ticket Information</a>
							</li>
							<li>
								<a href="#">Buy Tickets</a>
							</li>
							<li>
								<a href="#">Stadium Plan</a>
							</li>
							<li>
								<a href="#">Conditions of Sale</a>
							</li>
							<li>
								<a href="#">Memberships</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#" class="heading">Team</a>
						<ul class="list-unstyled sub-links">
							<li>
								<a href="#">Player Profiles</a>
							</li>
							<li>
								<a href="#">Staff Profiles </a>
							</li>
							<li>
								<a href="#">Academy Profiles</a>
							</li>
							<li>
								<a href="#">Academy Staff</a>
							</li>
						</ul>						
					</li>
					<li>
						<a href="#" class="heading">Stats</a>
						<ul class="list-unstyled sub-links">
							<li>
								<a href="#">Head to Head Stats</a>
							</li>
							<li>
								<a href="#">Player Stats</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#" class="heading">Fans</a>
						<ul class="list-unstyled sub-links">
							<li>
								<a href="#">Disabled Supporters Club</a>
							</li>
							<li>
								<a href="#">Official Wolves Merchandise</a>
							</li>
							<li>
								<a href="#">Fans Parliament</a>
							</li>
							<li>
								<a href="#">Youth Parliament</a>
							</li>
							<li>
								<a href="#">My Wolves</a>
							</li>
							<li>
								<a href="#">Junior Supporters</a>
							</li>
							<li>
								<a href="#">Wolves Products</a>
							</li>
							<li>
								<a href="#">Stadium Tours</a>
							</li>
						</ul>						
					</li>
					<li>
						<a href="#" class="heading">Club</a>
						<ul class="list-unstyled sub-links">
							<li>
								<a href="#">Business Finder</a>
							</li>
							<li>
								<a href="#">About Us</a>
							</li>
							<li>
								<a href="#">Wolves Community Trust</a>
							</li>
							<li>
								<a href="#">Museum</a>
							</li>
							<li>
								<a href="#">Safeguarding</a>
							</li>
							<li>
								<a href="#">Vacancies</a>
							</li>
							<li>
								<a href="#">Contact Us</a>
							</li>
							<li>
								<a href="#">Your Feedback</a>
							</li>
						</ul>						
					</li>
					<li>
						<a href="#" class="heading">Commercial</a>
						<ul class="list-unstyled sub-links">
							<li>
								<a href="#">Matchday Hospitality</a>
							</li>
							<li>
								<a href="#">Seasonal Hospitality</a>
							</li>
							<li>
								<a href="#">Conference/Private Hire</a>
							</li>
							<li>
								<a href="#">Christmas Events</a>
							</li>
							<li>
								<a href="#">Advertising/Sponsorship</a>
							</li>
							<li>
								<a href="#">Contact Corporate Wolves</a>
							</li>
						</ul>						
					</li>
				</ul>
			</div>
			<div class="copyright">
				<div class="col-md-12">
					<p>All Rights Reserved</p>
				</div>
			</div>
		</div>
	</footer>
	<!-- End: footer -->

	<!-- Scripts -->
	<script type="text/javascript" src="{{ asset( 'js/jquery.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/bootstrap.min.js' ) }}"></script>
</body>
</html>