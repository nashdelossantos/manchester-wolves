@extends( 'tpl.main' )

@section( 'styles' )
<link rel="stylesheet" type="text/css" href="{{ asset( 'css/page.css' ) }}">
@endsection

@section( 'pagecontent' )
<section class="content-article" id="about-page">
	<div class="container">

	<div class="title-box">
		<h3>{{ $data['content']['heading'] }}</h3>
	</div>

	<div class="black-box">
		<div class="col-md-12">
			<h4 class="heading">{{ $data['content']['heading'] }}</h4>
			<div class="col-md-8">
				<article>
					{!! $data['content']['page_content'] !!}
				</article>	
			</div>
			<div class="col-md-4">
				<img src="{{ asset( 'img/about-pic.jpg' ) }}" class="img-responsive">
			</div>

		</div><div class="clearfix"></div>	
	</div>
	

	
	</div>
</section>
@endsection