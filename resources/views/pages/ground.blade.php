@extends( 'tpl.main' )

@section( 'styles' )
<link rel="stylesheet" type="text/css" href="{{ asset( 'css/page.css' ) }}">
@endsection

@section( 'pagecontent' )
<section class="content-article" id="about-page">
	<div class="container">

	<div class="title-box">
		<h3>{{ $data['content']['page_title'] }}</h3>
	</div>

	<div class="black-box">
		<div class="col-md-12">
			<h4 class="heading">{{ $data['content']['heading'] }}</h4>
			<div class="col-md-4">
				<div class="col-md-4">
				</div>	
				<div class="col-md-8">
					{!! $data['content']['page_content'] !!}
				</div>
			</div>
			<div class="col-md-8">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2375.7566627816536!2d-2.166719!3d53.454936!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487bb3fdad998f9f%3A0xcbd682b57940a871!2sAbbey+Hey+Football+Club!5e0!3m2!1sen!2suk!4v1434922200983" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

			</div>
				

		</div>	
		<div class="clearfix"></div>	
	</div>
	

	
	</div>
</section>
@endsection