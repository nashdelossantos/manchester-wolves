@extends( 'tpl.main' )

@section( 'styles' )
<link rel="stylesheet" type="text/css" href="{{ asset( 'css/page.css' ) }}">
@endsection

@section( 'pagecontent' )
<section class="content-article" id="about-page">
	<div class="container">

	<div class="title-box">
		<h3>{{ $data['content']['page_title'] }}</h3>
	</div>

	<div class="black-box">
		<div class="col-md-12">
			<h4 class="heading">{{ $data['content']['heading'] }}</h4>

			@foreach ($data['news'] as $news )
				<div class="news-post odd">
					<div class="col-md-2 img">
						<img src="{{ asset( 'img/newsimages/' ) }}/{{ $news->news_thumb }}" class="float">
					</div>
					<div class="col-md-10 news-desc">
						<a href="#"><h3 class="news-title">{{ $news->news_title }}
						<small class="pull-right"><i class="fa fa-calendar-o"></i> {{ date_format($news->updated_at, "d-m-Y") }}</small></h3></a>
						{!! $news->news_content !!}
					</div>	
					<div class="clearfix"></div>
				</div>
			@endforeach

			<!--
			<div class="news-post odd">
				<div class="col-md-2 img">
					<img src="{{ asset( 'img/news-1.jpg' ) }}" class="float">
				</div>
				<div class="col-md-10 news-desc">
					<a href="#"><h3 class="news-title">Lorem ipsum dolor sit amet
					<small class="pull-right"><i class="fa fa-calendar-o"></i> 4 days ago</small></h3></a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum...</p>
				</div>	
				<div class="clearfix"></div>
			</div>

			<div class="news-post even">
				<div class="col-md-2 img">
					<img src="{{ asset( 'img/news-2.jpg' ) }}" class="float">
				</div>
				<div class="col-md-10 news-desc">
					<a href="#"><h3 class="news-title">Lorem ipsum dolor sit amet
					<small class="pull-right"><i class="fa fa-calendar-o"></i> 4 days ago</small></h3></a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum...</p>
				</div>	
			</div>
			<div class="news-post odd">
				<div class="col-md-2 img">
					<img src="{{ asset( 'img/news-3.jpg' ) }}" class="float">
				</div>
				<div class="col-md-10 news-desc">
					<a href="#"><h3 class="news-title">Lorem ipsum dolor sit amet
					<small class="pull-right"><i class="fa fa-calendar-o"></i> 4 days ago</small></h3></a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum...</p>
				</div>	
			</div>
			-->
		</div><div class="clearfix"></div>	
	</div>
	

	
	</div>
</section>
@endsection