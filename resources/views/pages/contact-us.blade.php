@extends( 'tpl.main' )

@section( 'styles' )
<link rel="stylesheet" type="text/css" href="{{ asset( 'css/page.css' ) }}">
@endsection

@section( 'pagecontent' )
<section class="content-article" id="about-page">
	<div class="container">

	<div class="title-box">
		<h3>{{ $data['content']['page_title'] }}</h3>
	</div>

	<div class="black-box">
		<div class="col-md-12">
		<h4 class="heading">{{ $data['content']['heading'] }}</h4>

			<div class="col-md-6">	
				{!! $data['content']['page_content'] !!}
			</div>
			<div class="col-md-6">
				<img src="{{ url('img/contact-us.jpg') }}" class="img-responsive" alt="">
			</div>
			
		</div><div class="clearfix"></div>	
	</div>
</section>
@endsection