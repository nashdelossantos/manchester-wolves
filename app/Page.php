<?php namespace App; 

use Illuminate\Database\Eloquent\Model as Eloquent;

	class Page extends Eloquent {

		protected $fillable = [
			'slug', 
			'page_title', 
			'heading', 
			'page_content', 
			'meta_title', 
			'meta_description', 
			'meta_keywords',
			'active'
		];
	}

?>