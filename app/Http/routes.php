<?php

Route::get( '/' , 'BaseController@index');
Route::get('{page}', 'PageController@show');

Route::get('home', 'HomeController@index');

Route::get('admin/dashboard', 'AdminController@index');
Entrust::routeNeedsRole('/admin/*', ['admin', 'owner'], Redirect::to('/forbidden'), false);

Route::resource('/admin/categories', 'CategoriesController');
Route::resource('/admin/pages', 'AdminPagesController');
Route::resource('/admin/news', 'AdminNewsController');
Route::resource('/admin/settings', 'SettingsController');
Route::resource('/admin/profile', 'ProfileController');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
