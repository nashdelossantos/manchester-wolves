<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Setting;
use Redirect;

class SettingsController extends Controller {

	protected $pagename;

	public function __construct()
	{
		$this->pagename = 'settings';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$getsettings = Setting::orderBy('slug', 'asc')->get();

		return view('admin.settings.settings_index',
			[
				'pagename' 		=> $this->pagename,
				'settingslist'	=> $getsettings
			]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function edit($slug)
	{
		$getsetting = Setting::whereSlug($slug)->first();

		return view('admin.settings.settings_edit',
			[
				'pagename' 		=> $this->pagename,
				'setting'	=> $getsetting
			]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function update(Request $request, $slug)
	{
		$updatesetting = Setting::whereSlug($slug)->first();
		$active 		= $request->active == null ? 0 : 1;

		$now = date('Y-m-d H:i:s');

		$updatesetting->setting_name 	= $request->setting_name;
		$updatesetting->setting_value 	= $request->setting_value;
		$updatesetting->active 			= $active;
		$updatesetting->updated_at 		= $now;
		$updatesetting->save();

		return Redirect::to('/admin/settings');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
