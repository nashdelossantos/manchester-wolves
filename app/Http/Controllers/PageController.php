<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;
use App\Page;
use App\News;

class PageController extends BaseController {
	
	protected $pagename;

	public function show($slug)
	{
		$data = array();
		$data['content'] = Page::whereSlug($slug)->first();

		switch ($slug) {
			case 'news':

				$getnews = News::where('active', '=', 1)->orderBy('updated_at', 'asc')->get();
				$data['news'] = $getnews;
				break;
			
			default:
				# code...
				break;
		}

		return view('pages.'.$slug, 
			[
				'pagename' 	=> $slug,
				'data' 	=> $data
			]);
	}
}
