<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\User;
use Auth;
use Redirect;
use Hash;

class ProfileController extends Controller {

	protected $pagename;

	public function __construct()
	{
		$this->pagename = 'profile';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$getprofile = Auth::user();

		return view('admin.profile.profile_index',
			[
				'pagename' 	=> $this->pagename,
				'profile'	=> $getprofile
			]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$getprofile = Auth::user();

		return view('admin.profile.profile_edit',
			[
				'pagename' 	=> $this->pagename,
				'profile'	=> $getprofile
			]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  string  $request
	 * @return Response
	 */
	public function update(Request $request)
	{
		$getprofile = Auth::user();

		$getprofile->name 		= $request->name;
		$getprofile->email 		= $request->email;
		$getprofile->password 	= Hash::make($request->password);
		$getprofile->save();

		return Redirect::to('/admin/profile');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
