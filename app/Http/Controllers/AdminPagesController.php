<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Page;
use Redirect;

class AdminPagesController extends Controller {

	protected $pagename;

	public function __construct()
	{
		$this->pagename = 'pages';
	}

	public function generateRandomString($length = 3) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$getpages = Page::orderBy('slug', 'asc')->get();

		return view('admin.pages.pages_index', 
			[
				'pagename'		=> $this->pagename,
				'pagelist'	=> $getpages
			]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.pages.pages_create',
			[
				'pagename' => $this->pagename
			]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$pagetitle 		= $request->page_title;
		$slug 			= strtolower(str_replace(' ', '-', $pagetitle));
		$pagetitle 		= $request->page_title;
		$desc 			= $request->desc;
		$metatitle 		= $request->meta_title;
		$metadesc		= $request->meta_description;
		$metakeywords	= $request->meta_keywords;
		$active 		= $request->active == 'on' ? 1 : 0;

		$now = date('Y-m-d H:i:s');

		$newpage 					= new Page;
		$newpage->page_title 		= $pagetitle;
		$newpage->slug 				= $slug;
		$newpage->meta_title 		= $metatitle;
		$newpage->meta_description 	= $metadesc;
		$newpage->meta_keywords 	= $metakeywords;
		$newpage->desc		 		= $desc;
		$newpage->active		 	= $active;
		$newpage->created_at		= $now;
		$newpage->updated_at		= $now;
		$newpage->save();

		return Redirect::to('/admin/pages');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function show($slug)
	{
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function edit($slug)
	{
		$pagecontents = Page::whereSlug($slug)->first();

		return view('admin.pages.pages_edit', 
			[
				'pagename' 	=> $this->pagename,
				'page' 		=> $pagecontents
			]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function update(Request $request, $slug)
	{
		$active 		= $request->active == '' ? 0 : 1;

		$now = date('Y-m-d H:i:s');

		$updatepage 		= Page::whereSlug($slug)->first();

		$updatepage->meta_title 		= $request->meta_title;
		$updatepage->meta_description 	= $request->meta_description;
		$updatepage->page_title 		= $request->page_title;
		$updatepage->page_content 		= $request->page_content;
		$updatepage->active 			= $active;
		$updatepage->updated_at		= $now;
		$updatepage->save();

		return Redirect::to('/admin/pages');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function destroy(Page $category, $slug)
	{
		$category = Page::where('slug', '=', $slug);
		$category->delete();

		return Redirect::to('/admin/pages');
	}

}
