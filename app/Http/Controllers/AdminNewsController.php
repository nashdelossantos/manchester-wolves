<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\News;
use Redirect;

use Image;

class AdminNewsController extends Controller {

	protected $pagename;

	public function __construct()
	{
		$this->pagename = 'news';
	}

	public function generateRandomString($length = 3) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$getnews = News::orderBy('slug', 'asc')->get();

		return view('admin.news.news_index', 
			[
				'pagename'		=> $this->pagename,
				'newslist'		=> $getnews
			]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.news.news_create',
			[
				'pagename' => $this->pagename
			]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{

		$newstitle 		= $request->news_title;
		$slug 			= strtolower(str_replace(' ', '-', $newstitle));
		$newstitle 		= $request->news_title;
		$desc 			= $request->news_content;
		$metatitle 		= $request->meta_title;
		$metadesc		= $request->meta_description;
		$metakeywords	= $request->meta_keywords;
		$active 		= $request->active == 'on' ? 1 : 0;

		$now = date('Y-m-d H:i:s');

		$newnews 					= new News;
		$newnews->news_title 		= $newstitle;
		$newnews->slug 				= $slug;
		$newnews->news_content		= $desc;
		$newnews->meta_title 		= $metatitle;
		$newnews->meta_description 	= $metadesc;
		$newnews->meta_keywords 	= $metakeywords;
		$newnews->active		 	= $active;
		$newnews->created_at		= $now;
		$newnews->updated_at		= $now;

		if ($request->hasFile('news_thumb')) {
			$image = $request->file('news_thumb');
            $filename  = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('img/newsimages/' . $filename);
        
            Image::make($image->getRealPath())->resize(300, null, function ($constraint) {
            	$constraint->aspectRatio();
            })->save($path);
            
            //save filename to db
            $newnews->news_thumb = $filename;
		} 

		$newnews->save();

		return Redirect::to('/admin/news');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function show($slug)
	{
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function edit($slug)
	{
		$newscontents = News::whereSlug($slug)->first();

		return view('admin.news.news_edit', 
			[
				'pagename' 	=> $this->pagename,
				'news' 		=> $newscontents
			]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function update(Request $request, $slug)
	{
		$newstitle 		= $request->news_title;
		$active 		= $request->active == null ? 0 : 1;

		$now = date('Y-m-d H:i:s');

		$newnews 					= News::whereSlug($slug)->first();
		$newnews->news_title 		= $newstitle;
		$newnews->news_content		= $request->news_content;
		$newnews->meta_title 		= $request->meta_title;
		$newnews->meta_description 	= $request->meta_description;
		$newnews->meta_keywords 	= $request->meta_keywords;
		$newnews->active		 	= $active;
		$newnews->updated_at		= $now;

		if ($request->hasFile('news_thumb')) {
			$image = $request->file('news_thumb');
            $filename  = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('img/newsimages/' . $filename);
        
            Image::make($image->getRealPath())->resize(300, null, function ($constraint) {
            	$constraint->aspectRatio();
            })->save($path);
            
            //save filename to db
            $newnews->news_thumb = $filename;
		} 

		$newnews->save();

		return Redirect::to('/admin/news');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function destroy(News $category, $slug)
	{
		$category = News::where('slug', '=', $slug);
		$category->delete();

		return Redirect::to('/admin/pages');
	}

}
