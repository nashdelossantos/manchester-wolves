<?php namespace App; 

use Illuminate\Database\Eloquent\Model as Eloquent;

	class Setting extends Eloquent {

		protected $fillable = ['slug', 'setting_name', 'setting_value', 'active'];
	}

?>