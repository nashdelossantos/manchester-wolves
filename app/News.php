<?php namespace App; 

use Illuminate\Database\Eloquent\Model as Eloquent;

	class News extends Eloquent {

		protected $fillable = ['slug', 'news_thumb', 'news_title', 'news_content', 'meta_title', 'meta_keywords', 'meta_description', 'active'];
	}

?>