<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;
     
    use App\News;

    class NewsTableSeeder extends Seeder{

        public function run()
        {

            $now = date('Y-m-d H:i:s');

            $news = new News();
            $newpages = $news->create([
                'slug'          => 'lorem-ipsum-lorem',
                'news_thumb'    => 'news-1.jpg',
                'news_title'    => 'Lorem Ipsum',
                'news_content'  => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum...</p>',
                'meta_title'    => 'current news and updates',
                'meta_keywords' => 'news, updates',
                'meta_description' => 'news updates from manchester wolves',
                'active'        => 1,
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);

            $newpages = $news->create([
                'slug'          => 'lorem-ipsum-loremx',
                'news_thumb'    => 'news-2.jpg',
                'news_title'    => 'Lorem Ipsum',
                'news_content'  => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum...</p>',
                'meta_title'    => 'current news and updates',
                'meta_keywords' => 'news, updates',
                'meta_description' => 'news updates from manchester wolves',
                'active'        => 1,
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);

            $newpages = $news->create([
                'slug'          => 'lorem-ipsum-loremcc',
                'news_thumb'    => 'news-3.jpg',
                'news_title'    => 'Lorem Ipsum',
                'news_content'  => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum...</p>',
                'meta_title'    => 'current news and updates',
                'meta_keywords' => 'news, updates',
                'meta_description' => 'news updates from manchester wolves',
                'active'        => 1,
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);

        }
    }
?>