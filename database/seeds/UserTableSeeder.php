<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;
     
    use App\User;

    class UserTableSeeder extends Seeder{

        public function run()
        {

            $now = date('Y-m-d H:i:s');

            $user = new User();
            $userAdmin = $user->create([
                'name'          => 'admin',
                'email'         => 'info@codegap.co.uk',
                'role'          => 'admin',
                'password'      => Hash::make('pa55w0rd'),
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);

            $userModerator = $user->create([
                'name'          => 'mod',
                'email'         => 'mod@codegap.co.uk',
                'role'          => 'moderator',
                'password'      => Hash::make('password'),
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);

            $userClient = $user->create([
                'name'          => 'testclient',
                'email'         => 'client@codegap.co.uk',
                'role'          => 'user',
                'password'      => Hash::make('password'),
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);

        }
    }
?>