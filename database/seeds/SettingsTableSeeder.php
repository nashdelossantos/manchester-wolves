<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;
     
    use App\Setting;

    class SettingsTableSeeder extends Seeder{

        public function run()
        {

            $now = date('Y-m-d H:i:s');

            $setting = new Setting();
            $newsetting = $setting->create([
                'slug'          => 'site-name',
                'setting_name'  => 'Site Name',
                'setting_value' => 'Manchester Wolves',
                'active'        => 1,
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);
            $newsetting = $setting->create([
                'slug'          => 'logo',
                'setting_name'  => 'Logo',
                'setting_value' => 'logo.png',
                'active'        => 1,
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);
            $newsetting = $setting->create([
                'slug'          => 'facebook',
                'setting_name'  => 'Facebook',
                'setting_value' => 'http://www.facebook.com',
                'active'        => 1,
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);
            $newsetting = $setting->create([
                'slug'          => 'twitter',
                'setting_name'  => 'Twitter',
                'setting_value' => 'http://www.twitter.com',
                'active'        => 1,
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);
        }
    }
?>