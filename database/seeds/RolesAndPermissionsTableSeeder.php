<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;
     
    use App\Role;
    use App\User;
    use App\Permission;

    class RolesAndPermissionsTableSeeder extends Seeder{

        public function run()
        {

            $now = date('Y-m-d H:i:s');

            $owner = new Role();
            $owner->name         = 'owner';
            $owner->display_name = 'Project Owner'; // optional
            $owner->description  = 'User is the owner of a given project'; // optional
            $owner->save();

            $admin = new Role();
            $admin->name         = 'admin';
            $admin->display_name = 'User Administrator'; // optional
            $admin->description  = 'User is allowed to manage and edit other users'; // optional
            $admin->save();

            //assign roles
            $user = User::where('name', '=', 'admin')->first();
            $user->attachRole($owner);

            $user = User::where('name', '=', 'mod')->first();
            $user->attachRole($admin);

            //add permissions
            $createPost = new Permission();
            $createPost->name         = 'create-post';
            $createPost->display_name = 'Create Posts'; // optional

            // Allow a user to...
            $createPost->description  = 'create new posts'; // optional
            $createPost->save();

            $editUser = new Permission();
            $editUser->name         = 'edit-user';
            $editUser->display_name = 'Edit Users'; // optional
            // Allow a user to...
            $editUser->description  = 'edit existing users'; // optional
            $editUser->save();

            //attach permision to admin user
            $admin->attachPermission($createPost);

            //attach permission to owner
            $owner->attachPermissions(array($createPost, $editUser));

        }
    }
?>