<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;
     
    use App\Page;

    class PageTableSeeder extends Seeder{

        public function run()
        {

            $now = date('Y-m-d H:i:s');

            $page = new Page();
            $newpages = $page->create([
                'slug'          => 'about',
                'page_title'    => 'About Us',
                'heading'       => 'The History',
                'page_content'  => '<p>Manchester Wolves was born out of a conversation in Christies Hospital on Boxing Day 2014,between me Darryl Lee and my close friend Simon Lebovits.</p>
                    <p>He and I were chatting about our amateur football management as we had both been managing teams over the last few years and loved talking about the highs and lows.</p>
                    <p>Simon had managed his son`s team for many years and I had managed a number of teams in amateur football as well as taking teams around the world for Maccabi GB.</p>
                    <p>Whilst bemoaning the effort of managing open age teams I talked about the possibility of managing vets-as they tend to be more reliable.</p>
                    <p>It was even suggested between us that I would help him manage his sons team when I had any spare weeks as the Vets league has fixtures every other week.</p>
                    <p>And so I launched Manchester Wolves who hope to start their first season in the Cheshire Vets League in September 2015.There is a very sad postscript. Early in 2015 Simon lost his battle with the Big C leaving a wife and three kids. From that moment on I have missed our football banter but it has made me all the more driven to form Manchester Wolves.</p>',
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);

            $newpages = $page->create([
                'slug'          => 'news',
                'page_title'    => 'News',
                'heading'       => '',
                'page_content'   => '',
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);

            $newpages = $page->create([
                'slug'          => 'fixtures',
                'page_title'    => 'Fixtures',
                'heading'       => '',
                'page_content'   => '',
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);

            $newpages = $page->create([
                'slug'          => 'results',
                'page_title'    => 'Results',
                'heading'       => '',
                'page_content'   => '',
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);

            $newpages = $page->create([
                'slug'          => 'contact-us',
                'page_title'    => 'Contact Us',
                'heading'       => '',
                'page_content'   => '',
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);

            $newpages = $page->create([
                'slug'          => 'ground',
                'page_title'    => 'Our Ground',
                'heading'       => '',
                'page_content'   => '',
                'created_at'    => $now,
                'updated_at'    => $now,
            ]);
        }
    }
?>